- [How Do We Assemble Genomes? (Part 1/2)](#org551c780)
  - [The String Reconstruction Problem](#org406baa8)
  - [String Reconstruction as a Walk in the Overlap Graph](#org93cabbe)
  - [Another Graph for String Reconstruction](#org6cd88d2)
  - [Walking in the de Bruijn Graph](#orgfb8cbe0)
- [How Do We Assemble Genomes? (Part 2/2)](#org8179aa3)
  - [From Euler's Theorem to an Algorithm for Finding Eulerian Cycles](#org14dbe5f)
  - [CS: Reconstructing a String from the Paired de Bruijn Graph](#org6937b51)
  - [Assembling Genomes from Read-Pairs](#org3ad5ccf)
  - [CS: Maximal Non-Branching Paths in a Graph](#org010129c)
  - [Epilogue: Genome Assembly Faces Real Sequencing Data](#org8615c2d)
- [How Do We Sequence Antibiotics? (Part 1/2)](#orga72f2fd)
  - [How Do Bacteria Make Antibiotics?](#org46bbf64)
  - [CS: Generating the Theoretical Spectrum of a Peptide](#org3297d8c)
  - [Sequencing Antibiotics by Shattering Them into Pieces](#orgac55fd3)
  - [A Brute Force Algorithm for Cyclopeptide Sequencing](#org6573809)
  - [A Branch-and-Bound Algorithm for Cyclopeptide Sequencing](#org0b931f6)

BTW, I confused the terms `nodes` and `edges` during this chapter. In all (or nearly all) cases `nodes` means `parent nodes` and `edges` means `child nodes`.

And for fucks sake, this was painful!


<a id="org551c780"></a>

# How Do We Assemble Genomes? (Part 1/2)


<a id="org406baa8"></a>

## The String Reconstruction Problem

**Code Challenge**: Solve the String Composition Problem.

**Input**: An integer k and a string Text.

**Output**: Compositionk(Text) (the k-mers can be provided in any order).

```python
def stringcomposition(k, string):
    composition = []
    for i in range(len(string) - k + 1):
        pattern = string[i : i + k]
        composition.append(pattern)
    return composition

# Test
k = 5
string = "CAATCCAAC"

result = stringcomposition(k, string)
# Sort results
result = sorted(result)

for i in result:
    print(i)
```

    AATCC
    ATCCA
    CAATC
    CCAAC
    TCCAA

```python
# Dataset
with open("dataset_197_3.txt", "r") as f:
    lines = f.readlines()

k = int(lines[0])
string = lines[1].rstrip()

result = stringcomposition(k, string)
# Sort results
result = sorted(result)

# Write to txt file
txt = '\n'.join(result)
f = open('data.txt','w')
f.write(txt)
f.close()
```


<a id="org93cabbe"></a>

## String Reconstruction as a Walk in the Overlap Graph

**String Spelled by a Genome Path Problem.** Reconstruct a string from its genome path.

**Input**: A sequence path of k-mers Pattern1, … ,Patternn such that the last k - 1 symbols of Patterni are equal to the first k-1 symbols of Patterni+1 for 1 ≤ i ≤ n-1.

**Output**: A string Text of length k+n-1 such that the i-th k-mer in Text is equal to Patterni (for 1 ≤ i ≤ n).

```python
def genome_path_comp(strings):
    string = strings[0]
    k = len(strings[0])
    for i in range(len(strings) - 1):
        prefix = strings[i + 1][0 : k - 1]
        suffix = strings[i][1 : k]
        if prefix == suffix:
            string = string + strings[i + 1][k - 1: k]
    return string

# Test
strings = ["ACCGA",
           "CCGAA",
           "CGAAG",
           "GAAGC",
           "AAGCT"]
print(genome_path_comp(strings))
```

    ACCGAAGCT

```python
# Dataset
with open("dataset_198_3.txt", "r") as f:
    lines = f.readlines()

strings = [pattern.strip() for pattern in lines]
result = genome_path_comp(strings)

# Write to txt file
txt = result
f = open('data.txt','w')
f.write(txt)
f.close()
```

**Overlap Graph Problem**: Construct the overlap graph of a collection of k-mers.

**Input**: A collection Patterns of k-mers.

**Output**: The overlap graph Overlap(Patterns).

```python
def overlap_graph(patterns):
    adjacent = {}
    # Generate dictionary which contains all suffixes from patterns.
    for i in range(len(patterns)):
        adjacent[patterns[i][1 : ]] = [patterns[i]]
    # Test if a prefix is the same as stored suffix. If yes, append to dictionary.
    for i in range(len(patterns)):
        prefix = patterns[i][0 : -1]
        if prefix in adjacent:
            adjacent[prefix].append(patterns[i])
    # Remove key value pairs which have only one entry and thus discard patterns
    # which do not have a matching suffix.
    adjacent = dict((k, v) for k, v in adjacent.items() if len(v) > 1)
    return adjacent

# Test
patterns = ["ATGCG",
            "GCATG",
            "CATGC",
            "AGGCA",
            "GGCAT",
            "GGCAC"]
result = overlap_graph(patterns)

for i in result:
    print(result[i][0] + " -> "+ ", ".join(result[i][1 :]))
```

    GCATG -> CATGC
    CATGC -> ATGCG
    AGGCA -> GGCAT, GGCAC
    GGCAT -> GCATG

```python
# Dataset
with open("dataset_198_10.txt", "r") as f:
    lines = f.readlines()

patterns = [pattern.strip() for pattern in lines]
result = overlap_graph(patterns)

# Write to txt file
f = open('data.txt','w')
for i in result:
    f.write(result[i][0] + " -> "+ ", ".join(result[i][1 :]) + "\n")
f.close()
```


<a id="org6cd88d2"></a>

## Another Graph for String Reconstruction

**De Bruijn Graph from a String Problem**: Construct the de Bruijn graph of a string.

**Input**: An integer k and a string Text.

**Output**: DeBruijnk(Text).

```python
def debrujin_graph(k, text):
    # Dictionary which will contain nodes.
    nodes = {}
    # Loop over k-mers.
    for i in range(len(text) - k + 1):
        pattern = text[i : i + k]
        # If k-mer's prefix is already in nodes, append its suffix.
        if pattern[: -1] in nodes:
            nodes[pattern[: -1]].append(pattern[1 :])
        # Otherwise just generate a new entry for prefix - suffix pair.
        else:
            nodes[pattern[: -1]] = [pattern[1 :]]
    return nodes

# Test
k = 4
text = "AAGATTCTCTAAGA"

result = debrujin_graph(k, text)
for i in sorted(result):
    print(i + " -> "+ ", ".join(result[i]))
```

    AAG -> AGA, AGA
    AGA -> GAT
    ATT -> TTC
    CTA -> TAA
    CTC -> TCT
    GAT -> ATT
    TAA -> AAG
    TCT -> CTC, CTA
    TTC -> TCT

```python
# Dataset
with open("dataset_199_6.txt", "r") as f:
    lines = f.readlines()

k = int(lines[0])
text = lines[1].rstrip()
result = debrujin_graph(k, text)

# Write to txt file
f = open('data.txt','w')
for i in sorted(result):
    f.write(i + " -> "+ ", ".join(result[i]) + "\n")
f.close()
```


<a id="orgfb8cbe0"></a>

## Walking in the de Bruijn Graph

**DeBruijn Graph from k-mers Problem**: Construct the de Bruijn graph from a set of k-mers.

**Input**: A collection of k-mers Patterns.

**Output**: The adjacency list of the de Bruijn graph DeBruijn(Patterns).

```python
def debruijn_graph_kmer(patterns):
    nodes = {}
    for i in range(len(patterns)):
        if patterns[i][: -1] in nodes:
            nodes[patterns[i][: -1]].append(patterns[i][1 :])
        else:
            nodes[patterns[i][: -1]] = [patterns[i][1 :]]
    return nodes

#Test
patterns = ["GAGG",
            "CAGG",
            "GGGG",
            "GGGA",
            "CAGG",
            "AGGG",
            "GGAG"]

result = debruijn_graph_kmer(patterns)

for i in sorted(result):
    print(i + " -> "+ ", ".join(result[i]))
```

    AGG -> GGG
    CAG -> AGG, AGG
    GAG -> AGG
    GGA -> GAG
    GGG -> GGG, GGA

```python
# Dataset
with open("dataset_200_8.txt", "r") as f:
    lines = f.readlines()

patterns = [pattern.strip() for pattern in lines]
result = debruijn_graph_kmer(patterns)

# Write to txt file
f = open('data.txt','w')
for i in sorted(result):
    f.write(i + " -> "+ ", ".join(result[i]) + "\n")
f.close()
```


<a id="org8179aa3"></a>

# How Do We Assemble Genomes? (Part 2/2)


<a id="org14dbe5f"></a>

## From Euler's Theorem to an Algorithm for Finding Eulerian Cycles

**Code Challenge**: Solve the Eulerian Cycle Problem.

**Input**: The adjacency list of an Eulerian directed graph.

**Output**: An Eulerian cycle in this graph.

This was actually quiet painful to implement &#x2026;

```python
from random import choice
from collections import deque

def generate_path_cycle(graph, start, stack):
    cur_node = start
    # Generate the rest of the path
    while True:
        nex_node = graph[cur_node][0]
        stack.append(cur_node)
        graph[cur_node].remove(nex_node)
        cur_node = nex_node
        if len(graph[cur_node]) == 0:
            break
    return graph, stack, cur_node

def generate_cycle(graph, cur_node, stack, cycle):
    while len(graph[cur_node]) == 0:
        # If the stack is empty, break, we are done
        if len(stack) == 0:
            break
        cur_node = stack.pop()
        cycle.appendleft(cur_node)
        if len(graph[cur_node]) != 0:
            break
    return graph, cur_node, stack, cycle

def eulerian_cycle(graph):
    # Generate a path through the graph
    # First, import the graph and separate the nodes and the edges
    edges = []
    nodes = []
    for key, values in graph.items():
        nodes.append(key)
        if len(values) == 1:
            for i in range(len(values)):
                edges.append(values[i])
        else:
            for i in range(len(values)):
                edges.append(values[i])
    # Second, choose a random starting position
    cur_node = choice(nodes)
    # Generate random path through graph and store it
    # Where we store the visited edges and the final cycle
    cycle = deque([])
    stack = []

    while True:

        # Generate a path through the graph.
        graph, stack, cur_node = generate_path_cycle(graph, cur_node, stack)

        # Generate cycle of the explored edges.
        graph, cur_node, stack, cycle = generate_cycle(graph, cur_node, stack, cycle)

        # Because the outer loop is always TRUE we check here if all lists in
        # the dictionary are empty and if yes, we break and should be done.
        if all(len(value) == 0 for value in graph.values()):
            cycle.append(cur_node)
            break

    return cycle

pattern = ["0 -> 3",
           "1 -> 0",
           "2 -> 1,6",
           "3 -> 2",
           "4 -> 2",
           "5 -> 4",
           "6 -> 5,8",
           "7 -> 9",
           "8 -> 7",
           "9 -> 6"]

graph_dict = {}

for i in range(len(pattern)):
    string = pattern[i].split(" -> ")
    graph_dict[string[0]] = string[1].split(",")

graph = graph_dict
result = eulerian_cycle(graph)
print(*result, sep = "->")
```

    5->4->2->1->0->3->2->6->8->7->9->6->5

```python
# Dataset
with open("dataset_203_2.txt", "r") as f:
    lines = f.readlines()

pattern = [pattern.strip() for pattern in lines]

graph_dict = {}

for i in range(len(pattern)):
    string = pattern[i].split(" -> ")
    graph_dict[string[0]] = string[1].split(",")

graph = graph_dict
result = eulerian_cycle(graph)

# Write to txt file
f = open('data.txt','w')
f.write("->".join(result))
f.close()
```

**Code Challenge**: Solve the Eulerian Path Problem.

**Input**: The adjacency list of a directed graph that has an Eulerian path.

**Output**: An Eulerian path in this graph.

```python
from itertools import chain
from collections import deque

def generate_path(graph, start, stack, nodes):
    cur_node = start
    # Generate the rest of the path
    while True:
        nex_node = graph[cur_node][0]
        stack.append(cur_node)
        graph[cur_node].remove(nex_node)
        # Check if the next node is a dead end. If yes, save it and do not
        # travel there.
        if nex_node not in nodes:
            end_node = nex_node
            cur_node = cur_node
        else:
            cur_node = nex_node
        if len(graph[cur_node]) == 0:
            break
    return graph, stack, cur_node

def eulerian_path(graph):
    # Generate a path through the graph
    # First, import the graph and separate the nodes and the edges
    edges = []
    nodes = []
    for key, values in graph.items():
        nodes.append(key)
        if len(values) == 1:
            for i in range(len(values)):
                edges.append(values[i])
        else:
            for i in range(len(values)):
                edges.append(values[i])
    # Second, find starting node which has an out-degree of 1 greater than its
    # in degree.
    for node, edges in graph.items():
        n_out = 0
        n_in = 0

        n_out = len(edges)
        n_in = sum(value == node for value in list(chain(*graph.values())))
        diff_start = n_out - n_in
        if diff_start == 1:
            start_node = node
        for i in range(len(edges)):
            if edges[i] not in nodes:
                end_node = edges[i]

    cur_node = start_node
    path = deque([])
    stack = []

    while True:

        # Generate a path through the graph.
        graph, stack, cur_node = generate_path(graph, cur_node, stack, nodes)

        # Generate cycle of the explored edges.
        graph, cur_node, stack, path = generate_cycle(graph, cur_node, stack, path)

        # Because the outer loop is always TRUE we check here if all lists in
        # the dictionary are empty and if yes, we break and should be done.
        if all(len(value) == 0 for value in graph.values()):
            path.append(end_node)
            break

    return path

pattern = ["0 -> 2",
           "1 -> 3",
           "2 -> 1",
           "3 -> 0,4",
           "6 -> 3,7",
           "7 -> 8",
           "8 -> 9",
           "9 -> 6"]

graph_dict = {}

for i in range(len(pattern)):
    string = pattern[i].split(" -> ")
    graph_dict[string[0]] = string[1].split(",")

graph = graph_dict
result = eulerian_path(graph)
print(*result, sep = "->")
```

    6->7->8->9->6->3->0->2->1->3->4

```python
# Dataset
with open("dataset_203_6.txt", "r") as f:
    lines = f.readlines()

pattern = [pattern.strip() for pattern in lines]

graph_dict = {}

for i in range(len(pattern)):
    string = pattern[i].split(" -> ")
    graph_dict[string[0]] = string[1].split(",")

graph = graph_dict
result = eulerian_path(graph)

# Write to txt file
f = open('data.txt','w')
f.write("->".join(result))
f.close()
```

**Code Challenge**: Solve the String Reconstruction Problem.

**Input**: An integer k followed by a list of k-mers Patterns.

**Output**: A string Text with k-mer composition equal to Patterns. (If multiple answers exist, you may return any one.)

```python
def string_reconstruction(patterns, k):
    graph = debruijn_graph_kmer(patterns)
    path = eulerian_path(graph)
    text = genome_path_comp(path)
    return text

k = 4
patterns = ["CTTA",
            "ACCA",
            "TACC",
            "GGCT",
            "GCTT",
            "TTAC"]

# Test
result = string_reconstruction(patterns, k)
print(result)
```

    GGCTTACCA

```python
# Dataset
with open("dataset_203_7.txt", "r") as f:
    lines = f.readlines()

k = lines[0]
patterns = [pattern.strip() for pattern in lines[1:]]
result = string_reconstruction(patterns, k)

# Write to txt file
txt = result
f = open('data.txt','w')
f.write(txt)
f.close()
```

**Code Challenge**: Solve the k-Universal Circular String Problem.

**Input**: An integer k.

**Output**: A k-universal circular string.

```python
from itertools import product

def k_universal_string(k):
    # Generate possible 0, 1 combinations, depending on k
    combos = list(product(["0", "1"], repeat = k))
    # Transform into proper input for further processing
    combos = [list(i) for i in combos]
    kmers = []
    for i in combos:
        kmers.append("".join(i))
    # Generate DB graph
    graph = debruijn_graph_kmer(kmers)
    path = eulerian_cycle(graph)
    text = genome_path_comp(path)

    text = text[0 : len(text) - (k - 1)]

    return text

# Test
k = 4
result = k_universal_string(k)
print(result)
```

    1000010011010111

```python
# Dataset
with open("dataset_203_11.txt", "r") as f:
    lines = f.readlines()

k = int(lines[0])
result = k_universal_string(k)

# Write to txt file
txt = result
f = open('data.txt','w')
f.write(txt)
f.close()
```


<a id="org6937b51"></a>

## CS: Reconstructing a String from the Paired de Bruijn Graph

**Code Challenge**: Implement StringSpelledByGappedPatterns.

**Input**: Integers k and d followed by a sequence of (k, d)-mers (a1|b1), … , (an|bn) such that Suffix(ai|bi) = Prefix(ai+1|bi+1) for 1 ≤ i ≤ n-1.

**Output**: A string Text of length k + d + k + n - 1 such that the i-th (k, d)-mer in Text is equal to (ai|bi) for 1 ≤ i ≤ n (if such a string exists).

```python
def string_spelled_gapped_patterns(patterns, k, d):
    # Get first part of read pairs
    first_patterns = [i[0] for i in patterns]
    # Get second part of read pairs.
    last_patterns = [i[1] for i in patterns]
    # Construct prefix string.
    prefix_string = genome_path_comp(first_patterns)
    # Construct suffix string.
    suffix_string = genome_path_comp(last_patterns)
    # Check if prefix and suffix overlap.
    for i in range(k + d + 1, len(prefix_string)):
        if prefix_string[i] != suffix_string[i - k - d]:
            return "There is no string spelled by the gapped patterns."
    # Return prefix + last k + d symbols of suffix
    return prefix_string + suffix_string[-(k + d) :]

# Test
k = 4
d = 2
patterns = [["GACC", "GCGC"],
            ["ACCG", "CGCC"],
            ["CCGA", "GCCG"],
            ["CGAG", "CCGG"],
            ["GAGC", "CGGA"]]

result = string_spelled_gapped_patterns(patterns, k, d)
print(result)
```

    GACCGAGCGCCGGA

```python
# Dataset
with open("dataset_6206_4.txt", "r") as f:
    lines = f.readlines()

integers = lines[0].split(" ")
k = int(integers[0])
d = int(integers[1])
patterns = []
for pair in lines[1:]:
    patterns.append(pair.rstrip().split("|"))
result = string_spelled_gapped_patterns(patterns, k, d)

# Write to txt file
txt = result
f = open('data.txt','w')
f.write(txt)
f.close()
```


<a id="org3ad5ccf"></a>

## Assembling Genomes from Read-Pairs

**Code Challenge**: Solve the String Reconstruction from Read-Pairs Problem.

**Input**: Integers k and d followed by a collection of paired k-mers PairedReads.

**Output**: A string Text with (k, d)-mer composition equal to PairedReads.

```python
def debruijn_graph_pairs(patterns):
    nodes = {}
    for i in range(len(patterns)):
        # Similar as before but for the pairs, tuples are needed.
        nodes_gen = tuple([patterns[i][0][: -1], patterns[i][1][: -1]])
        edges_gen = tuple([patterns[i][0][1 :], patterns[i][1][1 :]])
        if nodes_gen in nodes:
            nodes[nodes_gen].append(edges_gen)
        else:
            nodes[nodes_gen] = [edges_gen]
    return nodes

def string_recomp_pairs(k, d, patterns):
    # Generate DeBruijn graph.
    graph = debruijn_graph_pairs(patterns)
    # Generate path through graph.
    path = eulerian_path(graph)
    # Compose string.
    string = string_spelled_gapped_patterns(path, k, d)

    return string

# Test
k = 4
d = 2
patterns = [["GAGA", "TTGA"],
            ["TCGT", "GATG"],
            ["CGTG", "ATGT"],
            ["TGGT", "TGAG"],
            ["GTGA", "TGTT"],
            ["GTGG", "GTGA"],
            ["TGAG", "GTTG"],
            ["GGTC", "GAGA"],
            ["GTCG", "AGAT"]]
result = string_recomp_pairs(k, d, patterns)
print(result)
```

    GTGGTCGTGAGATGTTGA

```python
# Dataset
with open("dataset_204_16.txt", "r") as f:
    lines = f.readlines()

integers = lines[0].split(" ")
k = int(integers[0])
d = int(integers[1])
patterns = []
for pair in lines[1:]:
    patterns.append(pair.rstrip().split("|"))
result = string_recomp_pairs(k, d, patterns)

# Write to txt file
txt = result
f = open('data.txt','w')
f.write(txt)
f.close()
```


<a id="org010129c"></a>

## CS: Maximal Non-Branching Paths in a Graph

**Code Challenge**: Implement MaximalNonBranchingPaths.

**Input**: The adjacency list of a graph whose nodes are integers.

**Output**: The collection of all maximal nonbranching paths in this graph.

```python
from itertools import chain
from collections import deque

def maximal_non_branching_paths(graph):
    # Store found non-branching paths.
    paths = []

    # Genearte nodes and edges.
    edges = []
    nodes = []
    for key, values in graph.items():
        nodes.append(key)
        if len(values) == 1:
            for i in range(len(values)):
                edges.append(values[i])
        else:
            for i in range(len(values)):
                edges.append(values[i])

    # Loop over all nodes.
    for node, edges in graph.items():
        n_out = 0
        n_in = 0

        n_out = len(edges)
        n_in = sum(value == node for value in list(chain(*graph.values())))
        # If current node is not a balanced node.
        if not(n_in == 1 and n_out == 1):
            # If the node has any outdoing connections.
            if n_out > 0:
                # If the current node is branching, loop over all connections.
                for i in edges:
                    stack = [node, i]
                    cur_node = i
                    if cur_node not in nodes:
                        n_out = 0
                    else:
                        n_out = len(graph[cur_node])
                    n_in = sum(value == cur_node for value in list(chain(*graph.values())))

                    while n_in == 1 and n_out == 1:
                        nex_node = graph[cur_node][0]
                        cur_node = nex_node
                        stack.append(cur_node)

                        if cur_node not in nodes:
                            n_out = 0
                        else:
                            n_out = len(graph[cur_node])
                        n_in = sum(value == cur_node for value in list(chain(*graph.values())))
                    paths.append(stack)


    used = []

    for node, edges in graph.items():
        if node not in used:
            start = node
            flag = False

            n_out = len(edges)
            n_in = sum(value == node for value in list(chain(*graph.values())))

            cur_node = start
            stack = []
            while n_in == 1 and n_out == 1:
                nex_node = graph[cur_node][0]
                used.append(cur_node)
                stack.append(cur_node)
                if nex_node == start:
                    flag = True
                    used.append(nex_node)
                    stack.append(nex_node)
                    break
                cur_node = nex_node

                if cur_node not in nodes:
                    n_out = 0
                else:
                    n_out = len(graph[cur_node])
                n_in = sum(value == cur_node for value in list(chain(*graph.values())))
            if flag == True:
                paths.append(stack)

    return paths

pattern = ["1 -> 2",
           "2 -> 3",
           "3 -> 4,5",
           "6 -> 7",
           "7 -> 6"]

graph_dict = {}

for i in range(len(pattern)):
    string = pattern[i].split(" -> ")
    graph_dict[string[0]] = string[1].split(",")

graph = graph_dict

result = maximal_non_branching_paths(graph)
for i in result:
    print(*i, sep = " -> ")

```

    1 -> 2 -> 3
    3 -> 4
    3 -> 5
    6 -> 7 -> 6

```python
# Dataset
with open("dataset_6207_2.txt", "r") as f:
    lines = f.readlines()

pattern = [pattern.strip() for pattern in lines]

graph_dict = {}

for i in range(len(pattern)):
    string = pattern[i].split(" -> ")
    graph_dict[string[0]] = string[1].split(",")

graph = graph_dict
# print(graph)

result = maximal_non_branching_paths(graph)
# Write to txt file
f = open('data.txt','w')
for i in result:
    print(*i, sep = " -> ", file = f)
f.close()
```


<a id="org8615c2d"></a>

## Epilogue: Genome Assembly Faces Real Sequencing Data

**Contig Generation Problem**: Generate the contigs from a collection of reads (with imperfect coverage).

**Input**: A collection of k-mers Patterns.

**Output**: All contigs in DeBruijn(Patterns).

```python
def contig_gen(patterns):
    # Generate DeBruijn graph.
    graph = debruijn_graph_kmer(patterns)

    # Generate non-branching paths
    paths = maximal_non_branching_paths(graph)

    # Generate contigs
    contigs = []
    for i in paths:
        contigs.append(genome_path_comp(i))

    return contigs

# Test
patterns = ["ATG",
            "ATG",
            "TGT",
            "TGG",
            "CAT",
            "GGA",
            "GAT",
            "AGA"]

result = contig_gen(patterns)
print(result)
```

    ['ATG', 'ATG', 'TGT', 'TGGA', 'CAT', 'GAT', 'AGA']

```python
# Dataset
with open("dataset_205_5.txt", "r") as f:
    lines = f.readlines()

patterns = [pattern.strip() for pattern in lines]

result = contig_gen(patterns)

# Write to txt file
f = open('data.txt','w')
print(*result, file = f, sep = " ")
f.close()
```


<a id="orga72f2fd"></a>

# How Do We Sequence Antibiotics? (Part 1/2)


<a id="org46bbf64"></a>

## How Do Bacteria Make Antibiotics?

**Protein Translation Problem**: Translate an RNA string into an amino acid string.

**Input**: An RNA string Pattern and the array GeneticCode.

**Output**: The translation of Pattern into an amino acid string Peptide.

```python
def rna_to_ac(pattern, genetic_code):
    k = 3
    peptide = []
    # Check if pattern is divisible by 3
    # if len(pattern) % k != 0:
    #     # print("RNA strand does not have required length!")
    #     # return
    # Calulate starting positons
    num_k = int(len(pattern) / k)
    # Slide over pattern string.
    for i in range(num_k):
        j = i * k
        # Get codon.
        codon = pattern[j : j + k]
        # Check if codon is STOP codon. If yes, skip.
        if genetic_code[codon] != "*":
            peptide.append(genetic_code[codon])

    return "".join(peptide)

# Stop codons are indicated by "*"
genetic_code = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
                "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
                "UAU":"Y", "UAC":"Y", "UAA":"*", "UAG":"*",
                "UGU":"C", "UGC":"C", "UGA":"*", "UGG":"W",
                "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
                "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
                "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
                "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
                "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
                "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
                "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
                "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
                "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
                "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
                "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
                "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G"}

# Test
pattern = "AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA"
result = rna_to_ac(pattern, genetic_code)
print(result)
```

    MAMAPRTEINSTRING

```python
# Dataset
with open("dataset_96_4.txt", "r") as f:
    lines = f.readlines()

pattern = lines[0]

result = rna_to_ac(pattern, genetic_code)

# Write to txt file
f = open('data.txt','w')
print(result, file = f)
f.close()
```

**Peptide Encoding Problem**: Find substrings of a genome encoding a given amino acid sequence.

**Input**: A DNA string Text, an amino acid string Peptide, and the array GeneticCode.

**Output**: All substrings of Text encoding Peptide (if any such substrings exist).

```python
# Helper functions
def Reverse(Pattern):
    rev = ""
    for char in Pattern:
        rev = char + rev
    return rev


def Complement(Pattern):
    comp = ""
    for char in Pattern:
        if char == "A":
            comp = comp + "T"
        if char == "C":
            comp = comp + "G"
        if char == "G":
            comp = comp + "C"
        if char == "T":
            comp = comp + "A"
    return comp


def ReverseComplement(Pattern):
    Pattern = Reverse(Pattern)
    Pattern = Complement(Pattern)
    return Pattern


# Main function
# Uses: ReverseComplement, rna_to_ac
def find_peptide_substrings(pattern, peptide, genetic_code):
    substrings = []
    # Calculate DNA k-mer length
    k = int(3 * len(peptide))
    # Slide over DNA and find k-mers and their reverse complements
    for i in range(len(pattern) - k + 1):
        dna = pattern[i : i + k]
        dna_rc = ReverseComplement(dna)
        # Transcribe into RNA
        rna = dna.replace("T", "U")
        rna_rc = dna_rc.replace("T", "U")
        # Translate into peptide sequence and check if normal or RC matches.
        if rna_to_ac(rna, genetic_code) == peptide or rna_to_ac(rna_rc, genetic_code) == peptide:
            substrings.append(dna)
    return substrings


# Test
pattern = "ATGGCCATGGCCCCCAGAACTGAGATCAATAGTACCCGTATTAACGGGTGA"
peptide = "MA"

result = find_peptide_substrings(pattern, peptide, genetic_code)
print(*result, sep = "\n")
```

    ATGGCC
    GGCCAT
    ATGGCC

```python
# Dataset
with open("dataset_96_7.txt", "r") as f:
    lines = f.readlines()

pattern = lines[0].rstrip()
peptide = lines[1].rstrip()

result = find_peptide_substrings(pattern, peptide, genetic_code)

# Write to txt file
f = open('data.txt','w')
print(*result, sep = "\n", file = f)
f.close()
```


<a id="org3297d8c"></a>

## CS: Generating the Theoretical Spectrum of a Peptide

**Code Challenge**: Implement LinearSpectrum.

**Input**: An amino acid string Peptide.

**Output**: The linear spectrum of Peptide.

```python
aminoacidmass = {"A" : 71,
                 "R" : 156,
                 "N" : 114,
                 "D" : 115,
                 "C" : 103,
                 "E" : 129,
                 "Q" : 128,
                 "G" : 57,
                 "H" : 137,
                 "I" : 113,
                 "L" : 113,
                 "K" : 128,
                 "M" : 131,
                 "F" : 147,
                 "P" : 97,
                 "S" : 87,
                 "T" : 101,
                 "W" : 186,
                 "Y" : 163,
                 "V" : 99}

def linear_spectrum(peptide, aminoacidmass):
    # Calculate prefix masses of peptide
    prefix_mass = []
    for i in range(len(peptide) + 1):
        seq = peptide[0 : i]
        mass = 0
        for j in seq:
            mass += aminoacidmass[j]
        prefix_mass.append(mass)

    # Calculate linear spectrum
    linearspec = [0]
    # Loop over whole peptide
    for i in range(len(peptide)):
        # Calculate difference of two prefix masses.
        for j in range(i + 1, len(peptide) + 1):
            linearspec.append(prefix_mass[j] - prefix_mass[i])
    return sorted(linearspec)

# Test
peptide = "NQEL"
result = linear_spectrum(peptide, aminoacidmass)
print(*result)
```

    0 113 114 128 129 242 242 257 370 371 484


<a id="orgac55fd3"></a>

## Sequencing Antibiotics by Shattering Them into Pieces

**Generating Theoretical Spectrum Problem**: Generate the theoretical spectrum of a cyclic peptide.

**Input**: An amino acid string Peptide.

**Output**: Cyclospectrum(Peptide).

```python
# Dataset
with open("dataset_4912_2.txt", "r") as f:
    lines = f.readlines()

peptide = lines[0].rstrip()

result = linear_spectrum(peptide, aminoacidmass)

# Write to txt file
f = open('data.txt','w')
print(*result, file = f)
f.close()
```

```python
def cyclic_spectrum(peptide, aminoacidmass):
    # Calculate prefix masses of peptide
    prefix_mass = []
    for i in range(len(peptide) + 1):
        seq = peptide[0 : i]
        mass = 0
        for j in seq:
            mass += aminoacidmass[j]
        prefix_mass.append(mass)

    # Get mass of whole peptide
    peptide_mass = prefix_mass[-1]

    # Calculate linear spectrum
    linearspec = [0]
    # Loop over whole peptide
    for i in range(len(peptide)):
        # Calculate difference of two prefix masses.
        for j in range(i + 1, len(peptide) + 1):
            linearspec.append(prefix_mass[j] - prefix_mass[i])
            # In order to get the masses for the subpeptides which wrap around
            # the ends we can substract the mass of the linear subpeptide from
            # the total mass of the whole peptide.
            if i > 0 and j < len(peptide):
                linearspec.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
    return sorted(linearspec)

# Test
peptide = "LEQN"
result = cyclic_spectrum(peptide, aminoacidmass)
print(*result)
```

    0 113 114 128 129 227 242 242 257 355 356 370 371 484

```python
# Dataset
with open("dataset_98_4.txt", "r") as f:
    lines = f.readlines()

peptide = lines[0].rstrip()

result = cyclic_spectrum(peptide, aminoacidmass)

# Write to txt file
f = open('data.txt','w')
print(*result, file = f)
f.close()
```


<a id="org6573809"></a>

## A Brute Force Algorithm for Cyclopeptide Sequencing

**Counting** Peptides with Given Mass Problem: Compute the number of peptides of given mass.

**Input**: An integer m.

**Output**: The number of linear peptides having integer mass m.

Recursive/naive solution from me, dynamic programming approach from net.

```python
# Integer masses of the amino acids. I/L and Q/K are counted as one.
aa_masses = [71, 156, 114, 115, 103, 129, 57, 137, 113, 128, 131, 147, 97, 87, 101, 186, 163, 99]

def recursive_aa_combinations(target, aa_masses, used, sequences):
    # Base cases
    # If sum of used amino acids weights corresponds to target weight, append
    # the used sequence to the sequences
    if sum(used) == target:
        sequences.append(used)
    # If the sum is bigger than the target, pass
    elif sum(used) > target:
        pass
    # Recursion
    # Loop over all amino acids weights and add them (each) to used sequence and
    # call recursive function.
    else:
        for aa in aa_masses:
            recursive_aa_combinations(target, aa_masses, used + [aa], sequences)
    # Return the list of amino acid combinations which result in target weight
    return sequences

print(len(recursive_aa_combinations(300, aa_masses, [], [])))

def dp_aa_combinations(target, aa_masses):
    # Generate an array to cache results. It has the length of the target mass
    # value + 1 and is initialized with zeroes.
    cache = [0]*(target + 1)
    # Initialize index variable. It has the value of the target mass.
    index = target
    # Set last value of cache to 1.
    cache[target] = 1
    # Loop as long as index is > 0
    while index > 0:
        # Loop over the amino acids
        for aa in aa_masses:
            # Here is where the magic happens. The difference of two aa masses
            # is used as an index in the cache. It's value is incremented by the
            # current index value. This means, in the first round, only
            # cache[index] is 1 and each result will be incremented by one. This
            # will continue until [index - aa] = 0 which means that one solution
            # has been found.
            # The caching is based on the fact that every entry in the cache is
            # the result of a previous calculation (index - aa).
            cache[index - aa] += cache[index]

        # Decrease index to progress
        index -= 1
        # Abort when index reaches zero.
        while cache[index] == 0:
            index -= 1

    # Return first entry of cache
    return cache[0]

print(dp_aa_combinations(800, aa_masses))
print(dp_aa_combinations(900, aa_masses))
print(dp_aa_combinations(1000, aa_masses))
print(dp_aa_combinations(1024, aa_masses))
print(dp_aa_combinations(1100, aa_masses))
print(dp_aa_combinations(1200, aa_masses))
print(dp_aa_combinations(1325, aa_masses))
print(dp_aa_combinations(1400, aa_masses))
print(dp_aa_combinations(1500, aa_masses))
print(dp_aa_combinations(1600, aa_masses))
```

    87
    39575132
    584340761
    8756135919
    14712706211
    131791563155
    1996626917844
    57835988090896
    464240419461956
    7106999769547482
    109000330554114810


<a id="org0b931f6"></a>

## A Branch-and-Bound Algorithm for Cyclopeptide Sequencing

**Exercise Break**: How many subpeptides does a linear peptide of given length n have? (Include the empty peptide and the entire peptide.)

**Input**: An integer n.

**Output**: The number of subpeptides of a linear peptide of length n.

```python
def num_subpeps_lin(n):
    number = (n * (n + 1) / 2) + 1
    return int(number)

print(num_subpeps_lin(4))
print(num_subpeps_lin(15608))
```

    11
    121812637

**Code Challenge**: Implement CyclopeptideSequencing

```python
aa_masses = [71, 156, 114, 115, 103, 129, 57, 137, 113, 128, 131, 147, 97, 87, 101, 186, 163, 99]
```

```python
def cyclo_spectrum(peptide):
    # remove leading zero
    peptide = peptide[1 :]
    # Calculate prefix masses of peptide
    prefix_mass = []
    for i in range(len(peptide) + 1):
        seq = peptide[0 : i]
        mass = 0
        for j in seq:
            mass += j
        prefix_mass.append(mass)
    # return prefix_mass

    # Get mass of whole peptide
    peptide_mass = prefix_mass[-1]
    # Calculate linear spectrum
    linearspec = [0]
    # Loop over whole peptide
    for i in range(len(peptide)):
        # Calculate difference of two prefix masses.
        for j in range(i + 1, len(peptide) + 1):
            linearspec.append(prefix_mass[j] - prefix_mass[i])
            # In order to get the masses for the subpeptides which wrap around
            # the ends we can substract the mass of the linear subpeptide from
            # the total mass of the whole peptide.
            if i > 0 and j < len(peptide):
                linearspec.append(peptide_mass - (prefix_mass[j] - prefix_mass[i]))
    return sorted(linearspec)

def cyclopeptide_sequencing(spectrum):
    # Set containing potential candidates. Initialized with only the "empty" peptide.
    candidate_peps = {(0,)}
    # List of final peptides.
    final_peps = []
    while len(final_peps) == 0:
        # Generate expanded peptides.
        # We want to use a set in order to avoid duplication. However, sets are
        # immutable. So we need to create a temporary empty set which will get
        # the expanded peptide sequence and then assign it to the original variable.
        tmp = set()
        for peptide in candidate_peps:
            for aa in aa_masses:
                candidate = peptide + (aa,)
                # Check if the candidate peptide is part of the spectrum. If
                # not, do not use it.
                if candidate[-1] in spectrum:
                    tmp.add(candidate)
        candidate_peps = tmp
        # Remove candiates whoes sum does not appear in the spectrum
        tmp = set()
        for peptide in candidate_peps:
            if sum(peptide) in spectrum:
                tmp.add(peptide)
        candidate_peps = tmp
        # Check if total sum of candidate is equal to spectrum total mass and if
        # its cyclic spectrum corresponds to the given spectrum.
        for peptide in candidate_peps:
            if sum(peptide) == spectrum[-1]:
                if cyclo_spectrum(peptide) == spectrum:
                    # Remove leading zero and append
                    final_peps.append(peptide[1 :])

    return final_peps

result = cyclopeptide_sequencing([0, 113, 128, 186, 241, 299, 314, 427])
tmp = []
for i in result:
    tmp.append("-".join(list(map(str, i))))
result = tmp
print(*result)
```

    128-186-113 186-128-113 186-113-128 113-186-128 113-128-186 128-113-186

```python
# Dataset
with open("dataset_100_6.txt", "r") as f:
    lines = f.readlines()

spectrum = lines[0].rstrip()
spectrum = list(map(int, spectrum.split(' ')))

result = cyclopeptide_sequencing(spectrum)

tmp = []
for i in result:
    tmp.append("-".join(list(map(str, i))))
result = tmp
# Write to txt file
f = open('data.txt','w')
print(*result, file = f)
f.close()
```
