- [How Do We Assemble Genomes? (Part 1/2)](#org4bdcca3)
  - [The String Reconstruction Problem](#org9b6c450)
  - [String Reconstruction as a Walk in the Overlap Graph](#org961d0fe)
  - [Another Graph for String Reconstruction](#org1533b60)
  - [Walking in the de Bruijn Graph](#orgc1030f8)



<a id="org4bdcca3"></a>

# How Do We Assemble Genomes? (Part 1/2)


<a id="org9b6c450"></a>

## The String Reconstruction Problem

**Code Challenge**: Solve the String Composition Problem.

**Input**: An integer k and a string Text.

**Output**: Compositionk(Text) (the k-mers can be provided in any order).

```python
def stringcomposition(k, string):
    composition = []
    for i in range(len(string) - k + 1):
        pattern = string[i : i + k]
        composition.append(pattern)
    return composition

# Test
k = 5
string = "CAATCCAAC"

result = stringcomposition(k, string)
# Sort results
result = sorted(result)

for i in result:
    print(i)
```

    AATCC
    ATCCA
    CAATC
    CCAAC
    TCCAA

```python
# Dataset
with open("dataset_197_3.txt", "r") as f:
    lines = f.readlines()

k = int(lines[0])
string = lines[1].rstrip()

result = stringcomposition(k, string)
# Sort results
result = sorted(result)

# Write to txt file
txt = '\n'.join(result)
f = open('data.txt','w')
f.write(txt)
f.close()
```


<a id="org961d0fe"></a>

## String Reconstruction as a Walk in the Overlap Graph

**String Spelled by a Genome Path Problem.** Reconstruct a string from its genome path.

**Input**: A sequence path of k-mers Pattern1, … ,Patternn such that the last k - 1 symbols of Patterni are equal to the first k-1 symbols of Patterni+1 for 1 ≤ i ≤ n-1.

**Output**: A string Text of length k+n-1 such that the i-th k-mer in Text is equal to Patterni (for 1 ≤ i ≤ n).

```python
def genome_path_comp(strings):
    string = strings[0]
    k = len(strings[0])
    for i in range(len(strings) - 1):
        prefix = strings[i + 1][0 : k - 1]
        suffix = strings[i][1 : k]
        if prefix == suffix:
            string = string + strings[i + 1][k - 1: k]
    return string

# Test
strings = ["ACCGA",
           "CCGAA",
           "CGAAG",
           "GAAGC",
           "AAGCT"]
print(genome_path_comp(strings))
```

    ACCGAAGCT

```python
# Dataset
with open("dataset_198_3.txt", "r") as f:
    lines = f.readlines()

strings = [pattern.strip() for pattern in lines]
result = genome_path_comp(strings)

# Write to txt file
txt = result
f = open('data.txt','w')
f.write(txt)
f.close()
```

**Overlap Graph Problem**: Construct the overlap graph of a collection of k-mers.

**Input**: A collection Patterns of k-mers.

**Output**: The overlap graph Overlap(Patterns).

```python
def overlap_graph(patterns):
    adjacent = {}
    # Generate dictionary which contains all suffixes from patterns.
    for i in range(len(patterns)):
        adjacent[patterns[i][1 : ]] = [patterns[i]]
    # Test if a prefix is the same as stored suffix. If yes, append to dictionary.
    for i in range(len(patterns)):
        prefix = patterns[i][0 : -1]
        if prefix in adjacent:
            adjacent[prefix].append(patterns[i])
    # Remove key value pairs which have only one entry and thus discard patterns
    # which do not have a matching suffix.
    adjacent = dict((k, v) for k, v in adjacent.items() if len(v) > 1)
    return adjacent

# Test
patterns = ["ATGCG",
            "GCATG",
            "CATGC",
            "AGGCA",
            "GGCAT",
            "GGCAC"]
result = overlap_graph(patterns)

for i in result:
    print(result[i][0] + " -> "+ ", ".join(result[i][1 :]))
```

    GCATG -> CATGC
    CATGC -> ATGCG
    AGGCA -> GGCAT, GGCAC
    GGCAT -> GCATG

```python
# Dataset
with open("dataset_198_10.txt", "r") as f:
    lines = f.readlines()

patterns = [pattern.strip() for pattern in lines]
result = overlap_graph(patterns)

# Write to txt file
f = open('data.txt','w')
for i in result:
    f.write(result[i][0] + " -> "+ ", ".join(result[i][1 :]) + "\n")
f.close()
```


<a id="org1533b60"></a>

## Another Graph for String Reconstruction

**De Bruijn Graph from a String Problem**: Construct the de Bruijn graph of a string.

**Input**: An integer k and a string Text.

**Output**: DeBruijnk(Text).

```python
def debrujin_graph(k, text):
    # Dictionary which will contain nodes.
    nodes = {}
    # Loop over k-mers.
    for i in range(len(text) - k + 1):
        pattern = text[i : i + k]
        # If k-mer's prefix is already in nodes, append its suffix.
        if pattern[: -1] in nodes:
            nodes[pattern[: -1]].append(pattern[1 :])
        # Otherwise just generate a new entry for prefix - suffix pair.
        else:
            nodes[pattern[: -1]] = [pattern[1 :]]
    return nodes

# Test
k = 4
text = "AAGATTCTCTAAGA"

result = debrujin_graph(k, text)
for i in sorted(result):
    print(i + " -> "+ ", ".join(result[i]))
```

    AAG -> AGA, AGA
    AGA -> GAT
    ATT -> TTC
    CTA -> TAA
    CTC -> TCT
    GAT -> ATT
    TAA -> AAG
    TCT -> CTC, CTA
    TTC -> TCT

```python
# Dataset
with open("dataset_199_6.txt", "r") as f:
    lines = f.readlines()

k = int(lines[0])
text = lines[1].rstrip()
result = debrujin_graph(k, text)

# Write to txt file
f = open('data.txt','w')
for i in sorted(result):
    f.write(i + " -> "+ ", ".join(result[i]) + "\n")
f.close()
```


<a id="orgc1030f8"></a>

## Walking in the de Bruijn Graph

**DeBruijn Graph from k-mers Problem**: Construct the de Bruijn graph from a set of k-mers.

**Input**: A collection of k-mers Patterns.

**Output**: The adjacency list of the de Bruijn graph DeBruijn(Patterns).

```python
def debruijn_graph_kmer(patterns):
    nodes = {}
    for i in range(len(patterns)):
        if patterns[i][: -1] in nodes:
            nodes[patterns[i][: -1]].append(patterns[i][1 :])
        else:
            nodes[patterns[i][: -1]] = [patterns[i][1 :]]
    return nodes

#Test
patterns = ["GAGG",
            "CAGG",
            "GGGG",
            "GGGA",
            "CAGG",
            "AGGG",
            "GGAG"]

result = debruijn_graph_kmer(patterns)

for i in sorted(result):
    print(i + " -> "+ ", ".join(result[i]))
```

    AGG -> GGG
    CAG -> AGG, AGG
    GAG -> AGG
    GGA -> GAG
    GGG -> GGG, GGA

```python
# Dataset
with open("dataset_200_8.txt", "r") as f:
    lines = f.readlines()

patterns = [pattern.strip() for pattern in lines]
result = debruijn_graph_kmer(patterns)

# Write to txt file
f = open('data.txt','w')
for i in sorted(result):
    f.write(i + " -> "+ ", ".join(result[i]) + "\n")
f.close()
```
